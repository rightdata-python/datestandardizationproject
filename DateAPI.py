import uvicorn
import fastapi
from typing import List, Optional
from fastapi import FastAPI, Query
from pydantic import BaseModel

from StepOneDate import dominant_DateFormat
from main import date
app = FastAPI()

class DateFormat(BaseModel):
    # Input file
    inputFile: "str"
    # Column Name 1
    columnName: "str"

class DateStandardization(BaseModel):
    # Input file
    inputFile: "str"
    # Column Name 1
    columnName: list
    dominantFormat: Optional[bool]
    # Output file storage location
    dateFormat: "str"
    outputFile: "str"
    defaultDate: Optional[str]
    sIncludeCol: Optional[list]

@app.post("/DateFormatAPI")
# calling the functions with given inputs
async def DateFormatAPI(LR1:DateFormat):
    Result = dominant_DateFormat(LR1.inputFile, LR1.columnName)
    # ...
    # Runs the DynamicLevenshtein
    # ...
    return {f"{Result}"}

@app.post("/DateStandardizationAPI")
# calling the functions with given inputs
async def DateStandardizeAPI(LR1:DateStandardization):
    Result = date(LR1.inputFile, LR1.columnName,LR1.dominantFormat, LR1.outputFile, LR1.dateFormat or 0, LR1.defaultDate,LR1.sIncludeCol)
    # ...
    # Runs the DynamicLevenshtein
    # ...
    return {f"{Result}"}


"""
{
  "inputFile": "C:\\Users\\nikhi\\OneDrive\\Desktop\\DateStandardization\\SAMPLE_FOR_DATEFORMAT.csv",
  "columnName": "Order Date1"
}
"""

"""
{
  "inputFile": "C:\\Users\\nikhi\\OneDrive\\Desktop\\DateStandardization\\SAMPLE_FOR_DATEFORMAT.csv",
  "columnName": ["Order Date1", "Order Date2"],
  "dominantFormat": true,
  "dateFormat": "%Y/%m/%d",
  "outputFile": "C:\\Users\\nikhi\\OneDrive\\Desktop\\DateStandardization\\APIResult.csv",
  "defaultFormat": "2012-02-02",
  "sIncludeCol": [
    "all"
  ]
}
"""
