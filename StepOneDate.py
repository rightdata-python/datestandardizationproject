#importing required packages
from __future__ import print_function
import pandas as pd
import datetime
import json
import os
from collections import OrderedDict
import mysql.connector as connection
import MySQLdb as my

def dominant_DateFormat(inputFile, columnName):

 dataset = pd.read_csv(inputFile)
 date_data = dataset[columnName]
 print("Total number of records: ", len(date_data))
 date_patterns_count = {}
 try:
     mysql_config = {}
     with open("mysql_db.json", "r") as db_config:                                          #reading from the json file containing database credentials
         mysql_config = json.load(db_config)
     mydb = connection.connect(host=mysql_config["host"], database=mysql_config['database'], user=mysql_config["user"], passwd=mysql_config["passwd"],
                               use_pure=True)
     query = "Select * FROM {0}.{1};".format(mysql_config['database'], mysql_config['table'])
     result_dataFrame = pd.read_sql(query, mydb)
     patterns_list = result_dataFrame.to_dict(orient="list")['DateFormat']                  #converting mySQL table to dataframe and then to dict
     for pattern in patterns_list:
         date_patterns_count[pattern] = 0                                                       #initializing count to 0
     mydb.close()  # close the connection
 except Exception as e:
     print(str(e))
     date_patterns_count = {'%Y-%m-%d':0,'%d-%m-%Y':0, '%B %Y':0, '%m-%d-%Y':0,
                        '%b %Y':0,'%Y/%m/%d':0, '%d/%m/%Y':0, '%m/%d/%Y':0,
                        '%m-%Y-%d':0,'%d-%Y-%m':0,'%m/%Y/%d':0, '%d/%Y/%m':0,
                        '%B %d %Y':0,'%b %d %Y':0,'%d-%m-%y':0,'%m-%d-%y':0,
                        '%d/%m/%y':0,'%m/%d/%y':0, '%A, %B %d, %Y':0,
                        'Others':0}

 for date in date_data:                                                                     #parsing throught column and incrementing count for each format found
     for format in date_patterns_count.keys():
         try:
             dates = datetime.datetime.strptime(date.strip(), format)
             date_patterns_count[format] += 1
             break
         except Exception as e:
             continue
     else:
         date_patterns_count['Others'] += 1
 response_msg = ""
 json_dict = OrderedDict()
 for index, format_count in enumerate(sorted(list(date_patterns_count.items()), key=lambda x:x[1], reverse=True)):
     if format_count[1] and index < 10:
         json_dict[format_count[0]] = format_count[1]
         response_msg += "There are {0} records in the {1} format,  ".format(format_count[1], format_count[0])

 out_str = json.dumps(json_dict)
 json_file_path = os.path.join(os.path.dirname(inputFile), "dominant_date_format.json")               #create json file and store in same location as the inputFiel
 jsonFile = open(json_file_path, "w")
 jsonFile.write(out_str)
 jsonFile.close()
 return response_msg.rstrip(", ")


# dominant_DateFormat("SAMPLE_FOR_DATEFORMAT.csv", "Order Date1")

