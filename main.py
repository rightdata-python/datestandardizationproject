#importing required packages
from dateutil.parser import parse
import pandas as pd
import datetime
import os
import json


def date(inputFile, columnName,dominant, outputFile, dateFormat, defaultDate, sIncludeCol):
 dataset = pd.read_csv(inputFile)                                                                  # reading as pandas dataframe
 columnsToInclude = columnName.copy()
 json_file_path = os.path.join(os.path.dirname(inputFile), "dominant_date_format.json")             # reading json file created by first API to find dominant format
 dominant_date = {}
 if os.path.isfile(json_file_path) and dominant:
     with open(json_file_path, "r") as json_date:
         dominant_date = json.load(json_date)
     for keys in dominant_date.keys():
         if keys != "Others":
             dateFormat = keys
             break

 if not defaultDate:
     defaultDate = ""
 else:
     defaultDate = defaultDate
 if sIncludeCol:
     for y in sIncludeCol:
         if y.lower() != "all":
             columnsToInclude.append(y)
 print(columnsToInclude)
 # Standardize all required columns
 col_list = list(dataset.columns.values)
 if (sIncludeCol and sIncludeCol[0].lower() != "all") or (not sIncludeCol):
     dataset = dataset[columnsToInclude]
     col_list = columnsToInclude

 for col in columnName:
     try:
         dataset["Standardized" + "_" + col] = dataset[[col]].apply(lambda row: standardize(*row, dateFormat, defaultDate),
                                                              axis=1)
     except Exception as e:
         print(str(e))
         return "Standardization process failed due to exception: " + str(e)

     index = col_list.index(col)
     col_list.insert(index+1, "Standardized" + "_" + col)
     print("col list", col_list)
 dataset = dataset[col_list]
 print(dataset)
 dataset.to_csv(outputFile, index=False)

def standardize(s1, dateFormat, defaultDate):
 if not dateFormat:
   dateFormat = '%Y-%m-%d'                                                   #if format not specified by user in API, convert to default format of year-month-date
 try:
   default_date = parse_no_default(str(s1).strip())
   if not default_date:          #checking for records that don't match the normal formats
       return defaultDate                                       #print error meesage if month or year is missing in record
   if default_date:
       dt = default_date
   else:
       dt = parse(str(s1))
   #print(str(dt.strftime(dateFormat)))
   return str(dt.strftime(dateFormat))
 except Exception as e:
  if "month must be in 1..12" in str(e):                                 # if following error occurs, exception handling by changing the format into a standard format and parsing the column again
      return date_exception_handling(["%Y-%d-%m", "%Y-%m-%d" , "%Y/%m/%d", "%Y/%d/%m"], str(s1), dateFormat, defaultDate)
  elif "day is out of range for month" in str(e):
      return date_exception_handling(["%m-%Y-%d", "%d-%Y-%m", "%m/%Y/%d", "%d/%Y/%m"], str(s1), dateFormat, defaultDate)
  else:
      return defaultDate                                             #return incorrect if records with unknown format exist

def date_exception_handling(format_list, s1, dateFormat, defaultDate):
    """
    function that implements the exception handling by checking for / and - first and
    converting to datetime objects and  to a correct standard format
    """
    for format in format_list:
        try:
            c = "/"
            if "-" in format:
                c = "-"
            mod_date = datetime.datetime.strptime(str(s1).strip(), format)
            new_date = str(mod_date.year) + c + str(mod_date.month) + c + str(mod_date.day)
            dt = parse(str(new_date))
            return str(dt.strftime(dateFormat))                            #standardizing format specified by the user
        except Exception as new:
            pass
    else:
        return defaultDate

def parse_no_default(dt_str):
  dt = parse(dt_str, default=datetime.datetime(1900, 1, 1))
  dt2 = parse(dt_str, default=datetime.datetime(1901, 2, 2))
  if dt == dt2:
    return dt
  else:
    if (dt.year == dt2.year and dt.month == dt2.month and dt.day !=dt2.day):
        return dt.replace(day=1)
    else:
        return None


date("SAMPLE_FOR_DATEFORMAT.csv", ["Order Date1", "Order Date2"], False, "DateResult.csv",'%Y-%m-%d', "2000-02-09", ["F"])




